var number_merchandise_result = 0;

window.onload = init;
    function init()
    {
        var node = document.getElementById('merchandise_panel');
        for(; number_merchandise_result<20; number_merchandise_result++)
        {
            var test = "";
            test += "merchandise-search-result_"
            test += number_merchandise_result;
            var merchandise_result = document.createElement('div');
            merchandise_result.id = test;
            merchandise_result.className = 'merchandise-search-result';
            merchandise_result.innerHTML = '<a href="product.html"><div class="merchandise-search-result-white-box"><p class="title">Kristina Dam Oak Table With White Marble Top</p><p class="price">$ 799.55</p></div></a>';  
            merchandise_result.style.backgroundColor = "none;";
            if(number_merchandise_result<10)
                merchandise_result.style.backgroundImage = "url(https://placeimg.com/30"+number_merchandise_result+"/480/any)"; 
            else 
                merchandise_result.style.backgroundImage = "url(https://placeimg.com/3"+number_merchandise_result+"/480/any)";            
            merchandise_result.style.marginRight = '30px';
            node.appendChild(merchandise_result);
        }
    }

function view_more() {
    var node = document.getElementById('merchandise_panel');
    var i = number_merchandise_result;
    for(; number_merchandise_result<(i+8); number_merchandise_result++)
    {
        var test = "";
        test += "merchandise-search-result_"
        test += number_merchandise_result;
        var merchandise_result = document.createElement('div');
        merchandise_result.id = test;
        merchandise_result.className = 'merchandise-search-result';
        merchandise_result.innerHTML = '<a href="product.html"><div class="merchandise-search-result-white-box"><p class="title">Kristina Dam Oak Table With White Marble Top</p><p class="price">$ 799.55</p></div></a>';  
        merchandise_result.style.backgroundColor = "none;";
        merchandise_result.style.backgroundImage = "url(https://placeimg.com/3"+number_merchandise_result+"/480/any)";            
        merchandise_result.style.marginRight = '30px';
        node.appendChild(merchandise_result);
    }
}

function color_replacement(id) {
    var node_id = id;
    var node = document.getElementById('test-color-panel');
        if(node_id == "cirle-1")
            node.style.backgroundColor = "#c7c6c5";
        if(node_id == "cirle-2")
            node.style.backgroundColor = "#43c0cf";
        if(node_id == "cirle-3")
            node.style.backgroundColor = "#3d402f";
        if(node_id == "cirle-4")
            node.style.backgroundColor = "#4aaf00";
        if(node_id == "cirle-5")
            node.style.backgroundColor = "#315bbd";
        if(node_id == "cirle-6")
            node.style.backgroundColor = "#ec6115";
        if(node_id == "cirle-7")
            node.style.backgroundColor = "#dc041d";
        if(node_id == "cirle-8")
            node.style.backgroundColor = "#FFFFFF";
}

function languageRU() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'RU<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function languageEN() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'EN<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function languageCH() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'CH<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function change_currency() {
    var node = document.getElementById('currency');
    if(node.innerHTML == '$US')
        node.innerHTML = 'RUR';
    else node.innerHTML = '$US';
}