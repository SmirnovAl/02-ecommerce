var number_merchandise_result = 0;

window.onload = init;
    function init()
    {
        var node = document.getElementById('merchandise_panel');
        for(; number_merchandise_result<4; number_merchandise_result++)
        {
            var test = "";
            test += "merchandise-search-result_"
            test += number_merchandise_result;
            var merchandise_result = document.createElement('div');
            merchandise_result.id = test;
            merchandise_result.className = 'merchandise-search-result';
            merchandise_result.innerHTML = '<a href="product.html"><div class="merchandise-search-result-white-box"><p class="title">Kristina Dam Oak Table With White Marble Top</p><p class="price">$ 799.55</p></div></a>';  
            merchandise_result.style.backgroundColor = "none;";
            merchandise_result.style.backgroundImage = "url(https://placeimg.com/30"+number_merchandise_result+"/480/any)";            
            merchandise_result.style.marginRight = '30px';
            node.appendChild(merchandise_result);
        }

        var notnode = document.getElementById('mini-screen-block');
        for(var i = 1; i<6; i++)
        {
            var mini_screen = document.createElement('li');
            mini_screen.innerHTML = '<div class="mini-version" id="mini-screen-'+i+'" onclick="change_big_image(id)"> </div>'; 
            mini_screen.style.marginBottom = '20px'            
            notnode.appendChild(mini_screen);     
        } 

        for(var i = 1; i<6; i++)
        {
            var div = document.getElementById('mini-screen-'+i+''); 
            div.style.backgroundImage = "url(https://placeimg.com/60"+i+"/600/any)";              
        }

        var big_screen = document.getElementById('big-screen');  
        big_screen.style.backgroundImage = "url(https://placeimg.com/60"+i+"/600/any)";      
    }

function view_more() {
    var node = document.getElementById('merchandise_panel');
    var i = number_merchandise_result;
    for(; number_merchandise_result<(i+8); number_merchandise_result++)
    {
        var test = "";
        test += "merchandise-search-result_"
        test += number_merchandise_result;
        var merchandise_result = document.createElement('div');
        merchandise_result.id = test;
        merchandise_result.className = 'merchandise-search-result';
        merchandise_result.innerHTML = '<a href="product.html"><div class="merchandise-search-result-white-box"><p class="title">Kristina Dam Oak Table With White Marble Top</p><p class="price">$ 799.55</p></div></a>';  
        merchandise_result.style.backgroundColor = "none;";
        merchandise_result.style.backgroundImage = "url(https://placeimg.com/30"+number_merchandise_result+"/480/any)";            
        merchandise_result.style.marginRight = '30px';
        node.appendChild(merchandise_result);
    }
}

var counter = 0;
function decrease() {
    var node = document.getElementById('counter');
    node.innerHTML = '';
    if(counter>0)
        counter= counter-1;
    node.innerHTML = counter;
}
function increase() {
    var node = document.getElementById('counter');
    node.innerHTML = '';
    counter= counter+1;
    node.innerHTML = counter;
}

function languageRU() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'RU<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function languageEN() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'EN<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function languageCH() {
    var node = document.getElementById('language');
    node.innerHTML = '';
    node.innerHTML = 'CH<span class="header-main-menu-language-triangle">&#9660;</span><ul class = "contact"><li onclick="languageRU()">RU </li><li onclick="languageEN()">EN </li><li onclick="languageCH()">CH</li></ul>'
}

function change_currency() {
    var node = document.getElementById('currency');
    if(node.innerHTML == '$US')
        node.innerHTML = 'RUR';
    else node.innerHTML = '$US';
}

function change_color(id) {
    var node_id = id;
    var node = document.getElementById('color-selection');
    if(node_id == "dark-gray") {
        node.innerHTML = '';    
        node.innerHTML = '<div class="select-panel-circle dark-gray"></div><p class="color-selection">Dark Gray</p><ul><li onclick="change_color(id)" id="dark-gray"><div class="select-panel-circle dark-gray"></div><p class="color-selection">Dark Gray</li><li onclick="change_color(id)" id="medium-gray"><div class="select-panel-circle medium-gray"></div><p class="color-selection">Medium Gray</li><li onclick="change_color(id)" id="light-gray"><div class="select-panel-circle light-gray"></div><p class="color-selection">Light Gray</li>';    
    }
    if(node_id == "medium-gray") {
        node.innerHTML = '';   
        node.innerHTML = '<div class="select-panel-circle medium-gray"></div><p class="color-selection">Medium Gray</p><ul><li onclick="change_color(id)" id="dark-gray"><div class="select-panel-circle dark-gray"></div><p class="color-selection">Dark Gray</li><li onclick="change_color(id)" id="medium-gray"><div class="select-panel-circle medium-gray"></div><p class="color-selection">Medium Gray</li><li onclick="change_color(id)" id="light-gray"><div class="select-panel-circle light-gray"></div><p class="color-selection">Light Gray</li>';    
    }
    if(node_id == "light-gray") {
        node.innerHTML = '';   
        node.innerHTML = '<div class="select-panel-circle light-gray"></div><p class="color-selection">Light Gray</p><ul><li onclick="change_color(id)" id="dark-gray"><div class="select-panel-circle dark-gray"></div><p class="color-selection">Dark Gray</li><li onclick="change_color(id)" id="medium-gray"><div class="select-panel-circle medium-gray"></div><p class="color-selection">Medium Gray</li><li onclick="change_color(id)" id="light-gray"><div class="select-panel-circle light-gray"></div><p class="color-selection">Light Gray</li>';
    }
}

function change_big_image(id) {
    var node_id = document.getElementById(id);
    var node = document.getElementById('big-screen');
    node.style.backgroundImage = node_id.style.backgroundImage;
}

var counter_purchase = 1;
function add_purchase() {
    if(counter_purchase == 1) {
        var list = document.getElementById('header_icons'); 
        var node = document.createElement('li');
        node.className = 'header-icons';
        node.id = "more-purchase"
        node.innerHTML = '<img class="counter" src="overall/images/header/ellipse.png" alt="Counter"/><p>'+counter_purchase+'</p>';
        list.insertBefore(node, list.children[4]);
        counter_purchase++;    }
    else if(counter_purchase > 1 && counter_purchase < 10) {
        var node = document.getElementById('more-purchase');
        node.innerHTML = '';
        node.innerHTML = '<img class="counter" src="overall/images/header/ellipse.png" alt="Counter"/><p>'+counter_purchase+'</p>';
        counter_purchase++;
    }
    else {
        var node = document.getElementById('more-purchase');
        node.innerHTML = '';
        node.innerHTML = '<img class="counter" src="overall/images/header/ellipse.png" alt="Counter"/><p>+</p>';
        counter_purchase++;        
    }

}

function more_description() {
    var list = document.getElementById('list'); 
    var node = document.createElement('li');
    node.className = 'product-description';
    node.id = "description-2"
    node.innerHTML = 'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium';
    list.insertBefore(node, list.children[5]);
    var remove_elem =  document.getElementById('read_more'); 
    remove_elem.remove();
}

